# Soundcloud Playlist App

<http://elv1s.ru/x/soundcloud-playlist-maker/>

## Story
✓ Create playlists of any tracks on SoundCloud  
✓ Store, edit and delete the playlists  
✓ Play all the tracks in the playlist with one click or tap  
✓ A user should be able to give a playlist a title and description

## Bonuses
□ Bonus points if a user can easily undo the last 100 actions. (some work started)  
□ Bonus points if it works on latest Opera(✓)/Opera Mobile(□).  
□ Bonus points for a bookmarklet that helps you add the tracks to the playlist from SoundCloud pages.

## TODO
□ Add tracks by their name/artist. Implement suggestion box for `playlists__add-track-input`.  
□ Human friendly URLs  
□ Decent music player. Progress bar is a must.
